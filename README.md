# Countdown

## Resources

-   https://incoherency.co.uk/countdown/practice/#

## Credits

-   Music from https://incoherency.co.uk
-   Letter frequencies - http://www.thecountdownpage.com/letters.htm

-   Favicons https://favicon.io/emoji-favicons/
