import React, { HTMLAttributes } from "react";

import Toolbar from "@material-ui/core/Toolbar";

import StickyFooter from "./StickyFooter";
import Footer from "components/Footer";
import NavigationBar from "components/navigationBar/NavigationBar";

interface IProps extends HTMLAttributes<HTMLElement> {}

export default function PageLayout({ children, ...rest }: IProps) {
    return (
        <StickyFooter footer={<Footer />} {...rest}>
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    flex: 1,
                }}
            >
                <NavigationBar />
                <Toolbar />
                {children}
            </div>
        </StickyFooter>
    );
}
