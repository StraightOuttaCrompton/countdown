import { HTMLAttributes, ReactNode } from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import useTheme from "@material-ui/core/styles/useTheme";
import clsx from "clsx";
import { useThemeContext } from "ThemeContext";

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            display: "flex",
            flex: "1 1 auto",
            flexDirection: "column",
            minHeight: "100vh",
        },
        content: {
            flex: "1 1 auto",
            display: "flex",
            color: theme.palette.text.primary,
        },
    })
);

interface IStickyFooterProps extends HTMLAttributes<HTMLElement> {
    footer: ReactNode;
}

export default function StickyFooter(props: IStickyFooterProps) {
    const classes = useStyles();
    const { children, footer, className, ...rest } = props;

    const theme = useTheme();
    const { dark } = useThemeContext();

    return (
        <main className={clsx(className, classes.root)} {...rest}>
            <div
                className={classes.content}
                style={{
                    backgroundColor: dark
                        ? theme.palette.background.default
                        : theme.palette.grey[200],
                }}
            >
                {children}
            </div>{" "}
            {footer}
        </main>
    );
}
