import React, { HTMLAttributes } from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import clsx from "clsx";

const useStyles = (maxWidth: number | "unset") =>
    makeStyles((theme) => {
        return createStyles({
            contentWrapper: {
                flex: "1 1 auto",
                display: "flex",
                justifyContent: "center",
            },
            maxWidthContainer: {
                flex: "1 1 auto",
                display: "flex",
                flexDirection: "column",
                maxWidth: maxWidth,
            },
            padding: {
                padding: theme.spacing(3),
                [theme.breakpoints.down("md")]: {
                    padding: theme.spacing(2),
                },
                [theme.breakpoints.down("xs")]: {
                    padding: theme.spacing(1),
                },
            },
        });
    });

interface IProps extends HTMLAttributes<HTMLElement> {
    disablePadding?: boolean;
    maxWidth?: number | "unset";
}

export default function PageSection(props: IProps) {
    const { disablePadding = false, maxWidth = 500, children, className, ...rest } = props;
    const classes = useStyles(maxWidth)();

    return (
        <div className={clsx(className, classes.contentWrapper)} {...rest}>
            <div
                className={clsx(classes.maxWidthContainer, {
                    [classes.padding]: !disablePadding,
                })}
            >
                {children}
            </div>
        </div>
    );
}
