import { useEffect, useRef } from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";

const colors = {
    lightGrey: "#7f7f7f",
    darkGrey: "#3b3838",
    darkBlue: "#225167",
    blue: "#1f4784",
    background: "#f1f1f1",
    white: "#f1f1f1",
    lightYellow: "rgba(255, 243, 0, 0.4)",
};

const layoutConsts = {
    outerRinglineWidth: 5,
    innerRinglineWidth: 7,
};

const useStyles = makeStyles((theme) => {
    return createStyles({
        countdownClock: {
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
        },
        clockContainer: {
            position: "relative",
            display: "flex",
        },
    });
});

interface IProps {
    currentTime: number;
    maxNumberOfSeconds: number;
    size?: number;
}

export default function CountdownClock({ currentTime, maxNumberOfSeconds, size = 250 }: IProps) {
    const classes = useStyles();

    let clockFaceRef = useRef<HTMLCanvasElement>(null);
    let clockHandRef = useRef<HTMLCanvasElement>(null);

    // https://www.geeksforgeeks.org/how-to-change-favicon-dynamically/
    useEffect(() => {
        const getFaviconLocation = (time: number): string => {
            const faviconNumber = Math.floor(time);

            return `${process.env.PUBLIC_URL}/favicons/${faviconNumber}/favicon.ico`;
        };

        getFaviconLocation(currentTime);

        const favicon = document.getElementById("favicon");
        if (favicon === null) return;

        const faviconLocation = getFaviconLocation(currentTime);

        favicon.setAttribute("href", faviconLocation);
    }, [currentTime]);

    useEffect(() => {
        let countdownTime = maxNumberOfSeconds - currentTime;
        if (countdownTime < 0) {
            countdownTime = 0;
        }

        const canvas = clockHandRef.current;
        if (canvas === null) return;

        const ctx = canvas.getContext("2d");
        if (ctx === null) return;

        /* parameters */
        const { width, height } = canvas;
        const midPoint = width / 2;

        ctx.clearRect(0, 0, width, height);

        const { outerRinglineWidth, innerRinglineWidth } = layoutConsts;

        const outerMargin = outerRinglineWidth + innerRinglineWidth;

        /* lit-up area */
        ctx.strokeStyle = colors.lightYellow;
        ctx.lineWidth = 7;
        for (let a = 0; a <= 30 - countdownTime; a++) {
            if (a % 15 === 0) continue;
            ctx.beginPath();
            ctx.moveTo(
                midPoint + (midPoint - outerMargin - 1) * Math.sin((Math.PI * 2 * a) / 60),
                midPoint - (midPoint - outerMargin - 1) * Math.cos((Math.PI * 2 * a) / 60)
            );
            ctx.lineTo(
                midPoint + (midPoint - outerMargin - 40) * Math.sin((Math.PI * 2 * a) / 60),
                midPoint - (midPoint - outerMargin - 40) * Math.cos((Math.PI * 2 * a) / 60)
            );
            ctx.stroke();
        }

        /* hand */
        const centerBallRadius = 10;
        const innerPadding = 2;
        ctx.fillStyle = colors.blue; // blue
        ctx.strokeStyle = colors.lightGrey; // grey
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.arc(
            midPoint,
            midPoint,
            centerBallRadius,
            (Math.PI * 2 * (-countdownTime + 8)) / 60,
            (Math.PI * 2 * (-countdownTime + 22)) / 60,
            true
        );
        ctx.lineTo(
            midPoint +
                (midPoint - outerMargin - innerPadding) *
                    Math.sin((Math.PI * 2 * countdownTime) / 60),
            midPoint +
                (midPoint - outerMargin - innerPadding) *
                    Math.cos((Math.PI * 2 * countdownTime) / 60)
        );
        ctx.fill();
        ctx.stroke();
    }, [currentTime, maxNumberOfSeconds]);

    useEffect(() => {
        const canvas = clockFaceRef.current;
        if (canvas === null) return;

        const ctx = canvas.getContext("2d");
        if (ctx === null) return;

        /* parameters */
        const { width, height } = canvas;
        const midPoint = width / 2;

        ctx.clearRect(0, 0, width, height);

        const { outerRinglineWidth, innerRinglineWidth } = layoutConsts;

        /* outer rings */
        ctx.beginPath();
        ctx.arc(midPoint, midPoint, midPoint - outerRinglineWidth, 0, 2 * Math.PI);
        ctx.strokeStyle = colors.lightGrey;
        ctx.lineWidth = outerRinglineWidth;
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(midPoint, midPoint, midPoint - innerRinglineWidth, 0, 2 * Math.PI);
        ctx.strokeStyle = colors.darkBlue;
        ctx.fillStyle = colors.background;
        ctx.lineWidth = innerRinglineWidth;
        ctx.fill();
        ctx.stroke();

        /* pips */
        const outerMargin = outerRinglineWidth + innerRinglineWidth;
        const innerPadding = 5;
        const lineLength = 30;
        const dotRadius = 2;

        ctx.strokeStyle = colors.darkGrey;
        ctx.lineWidth = 2;
        ctx.fillStyle = colors.white;

        for (let a = 0; a < 60; a += 5) {
            // grey line
            ctx.beginPath();
            ctx.moveTo(
                midPoint +
                    (midPoint - outerMargin - innerPadding) * Math.sin((Math.PI * 2 * a) / 60),
                midPoint +
                    (midPoint - outerMargin - innerPadding) * Math.cos((Math.PI * 2 * a) / 60)
            );
            ctx.lineTo(
                midPoint + (midPoint - outerMargin - lineLength) * Math.sin((Math.PI * 2 * a) / 60),
                midPoint + (midPoint - outerMargin - lineLength) * Math.cos((Math.PI * 2 * a) / 60)
            );
            ctx.stroke();

            // white dot
            ctx.beginPath();
            ctx.arc(
                midPoint + (midPoint - innerRinglineWidth) * Math.sin((Math.PI * 2 * a) / 60),
                midPoint + (midPoint - innerRinglineWidth) * Math.cos((Math.PI * 2 * a) / 60),
                dotRadius,
                0,
                2 * Math.PI
            );
            ctx.fill();
        }

        /* Background cross */
        ctx.beginPath();
        ctx.lineWidth = 3;
        ctx.moveTo(midPoint, outerMargin);
        ctx.lineTo(midPoint, width - outerMargin);
        ctx.moveTo(outerMargin, midPoint);
        ctx.lineTo(width - outerMargin, midPoint);
        ctx.stroke();
    }, []);

    return (
        <div className={classes.countdownClock}>
            <div className={classes.clockContainer}>
                <canvas ref={clockFaceRef} width={size} height={size} />
                <canvas
                    ref={clockHandRef}
                    width={size}
                    height={size}
                    style={{ position: "absolute" }}
                />
            </div>
        </div>
    );
}
