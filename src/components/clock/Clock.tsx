import React, { useState, useEffect } from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";

import clockCountdown30Audio from "assets/audio/clock-countdown-30.mp3";
import useInterval from "hooks/useInterval";

import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import PauseIcon from "@material-ui/icons/Pause";
import Paper from "@material-ui/core/Paper";

import StyledButton from "components/StyledButton";
import CountdownClock from "./CountdownClock";

const useStyles = makeStyles((theme) => {
    return createStyles({
        clock: {
            padding: theme.spacing(2),
        },
        buttonContainer: {
            display: "flex",
            justifyContent: "center",
            marginTop: theme.spacing(2),
            "& > *": {
                marginRight: theme.spacing(1),
                minWidth: 115,
                [theme.breakpoints.down("xs")]: {
                    flex: "1",
                },
            },
            "& > *:last-child": {
                marginRight: 0,
            },
        },
    });
});

const numberOfSeconds = 30;
const audio = new Audio(clockCountdown30Audio);

interface IProps {
    disabled?: boolean;
}

export default function Clock({ disabled = false }: IProps) {
    const classes = useStyles();

    const [isPaused, setIsPaused] = useState<boolean>(true);
    const [currentTime, setCurrentTime] = useState<number>(0);

    useEffect(() => {
        audio.pause();
        setIsPaused(true);

        audio.currentTime = 0;
        setCurrentTime(0);
    }, [disabled]);

    useInterval(
        () => {
            setCurrentTime(audio.currentTime);
        },
        isPaused ? null : 100
    );

    const toggleTimer = () => {
        const toggledPauseValue = !isPaused;

        if (toggledPauseValue) {
            audio.pause();
        } else {
            audio.play();
        }

        setIsPaused(toggledPauseValue);
    };

    const reset = () => {
        audio.pause();
        setIsPaused(true);

        audio.currentTime = 0;
        setCurrentTime(0);
    };

    return (
        <Paper className={classes.clock}>
            <CountdownClock currentTime={currentTime} maxNumberOfSeconds={numberOfSeconds} />

            <div className={classes.buttonContainer}>
                <StyledButton
                    color="primary"
                    onClick={toggleTimer}
                    disabled={currentTime > numberOfSeconds || disabled}
                    startIcon={isPaused ? <PlayArrowIcon /> : <PauseIcon />}
                >
                    {isPaused ? "Start" : "Pause"}
                </StyledButton>
                <StyledButton
                    onClick={reset}
                    disabled={(currentTime === 0 && isPaused) || disabled}
                >
                    Reset Timer
                </StyledButton>
            </div>
        </Paper>
    );
}
