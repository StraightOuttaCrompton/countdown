import React from "react";
import Button, { ButtonProps } from "@material-ui/core/Button";

export interface IStyledButtonProps extends ButtonProps {}

export default function StyledButton(props: IStyledButtonProps) {
    const { variant = "contained", ...rest } = props;

    return <Button variant={variant} {...rest} />;
}
