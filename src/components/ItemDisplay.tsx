import React from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import { useThemeContext } from "ThemeContext";

import Typography from "@material-ui/core/Typography";

const useStyles = (dark: boolean) =>
    makeStyles((theme) => {
        const letterBoxSize = 40;

        return createStyles({
            itemDisplay: {
                display: "flex",
                flexWrap: "wrap",
                justifyContent: "center",
            },
            itemContainer: {
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                border: `1px solid ${theme.palette.divider}`,
                backgroundColor: dark ? theme.palette.background.default : theme.palette.grey[200],
                minWidth: letterBoxSize,
                minHeight: letterBoxSize,
                textAlign: "center",
            },
            itemExists: {
                backgroundColor: dark ? theme.palette.grey[900] : theme.palette.grey[100],
            },
        });
    });

interface IProps {
    items: (string | undefined)[];
    maxNumberOfItems: number;
}

export default function ItemDisplay({ items, maxNumberOfItems }: IProps) {
    const { dark } = useThemeContext();
    const classes = useStyles(dark)();

    return (
        <div className={classes.itemDisplay}>
            {Array.from(new Array(maxNumberOfItems)).map((_, index) => {
                const item = items[index];

                return (
                    <div
                        className={clsx(classes.itemContainer, {
                            [classes.itemExists]: typeof item !== "undefined",
                        })}
                        key={index}
                    >
                        <Typography>{typeof item === "undefined" ? "" : item}</Typography>
                    </div>
                );
            })}
        </div>
    );
}
