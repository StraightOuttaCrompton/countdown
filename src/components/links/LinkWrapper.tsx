import React from "react";
import Link, { LinkProps } from "@material-ui/core/Link";

export interface ILinkWrapperProps extends LinkProps {}

export default function LinkWrapper({ style, ...rest }: ILinkWrapperProps) {
    return <Link color="secondary" style={{ cursor: "pointer", ...style }} {...rest} />;
}
