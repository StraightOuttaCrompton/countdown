import React from "react";
import LinkWrapper, { ILinkWrapperProps } from "./LinkWrapper";

interface IProps extends ILinkWrapperProps {}

export default function NewTabLink(props: IProps) {
    return <LinkWrapper target="_blank" rel="noreferrer" {...props} />;
}
