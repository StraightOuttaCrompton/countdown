import React from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";

import routes from "routes";
import GitLogoSvg from "assets/GitLogoSvg";

import NewTabLink from "components/links/NewTabLink";
import RouterLink from "components/links/RouterLink";

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            padding: theme.spacing(1),
            "& > *": {
                marginRight: theme.spacing(1),
            },
            "& > *:last-child": {
                marginRight: 0,
            },
            boxShadow: theme.shadows[4],
            backgroundColor: theme.palette.background.paper,
            color: theme.palette.text.primary,
        },
        creditsLink: {
            color: "inherit",
        },
        sourceCodeContainer: {
            display: "flex",
            alignItems: "center",
            color: "inherit",
        },
        gitLogo: {
            marginRight: theme.spacing(1),
        },
    })
);

export default function Footer() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <RouterLink to={routes.creditsPage} className={classes.creditsLink}>
                Credits
            </RouterLink>

            <NewTabLink
                href="https://gitlab.com/StraightOuttaCrompton/countdown"
                className={classes.sourceCodeContainer}
            >
                <GitLogoSvg className={classes.gitLogo} />
                Source Code
            </NewTabLink>
        </div>
    );
}
