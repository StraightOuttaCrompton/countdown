import React from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import { useThemeContext } from "ThemeContext";
import routes from "routes";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";

import Brightness3Icon from "@material-ui/icons/Brightness3";
import WbSunnyIcon from "@material-ui/icons/WbSunny";

import NavigationItem from "./NavigationItem";
import INavigationItem from "./models/INavigationItem";

const useStyles = makeStyles((theme) => {
    return createStyles({
        desktopNavItemsContainer: {
            display: "flex",
        },
    });
});

export default function NavigationBar() {
    const classes = useStyles();

    const { dark: isDarkTheme, toggle: onDarkThemeToggle } = useThemeContext();

    const navItems: INavigationItem[] = [
        { label: "Letters", route: routes.lettersGame, matchExactPath: true },
        { label: "Numbers", route: routes.numbersGame, matchExactPath: true },
    ];

    return (
        <AppBar position="fixed">
            <Toolbar style={{ alignItems: "unset", justifyContent: "space-between" }}>
                <List component="nav" className={classes.desktopNavItemsContainer} disablePadding>
                    {navItems.map((item, i) => {
                        return (
                            <React.Fragment key={i}>
                                <NavigationItem item={item} />
                            </React.Fragment>
                        );
                    })}
                </List>

                <div style={{ display: "flex", alignItems: "center" }}>
                    <Tooltip title="toggle dark mode">
                        <IconButton
                            color="inherit"
                            aria-label="toggle dark mode"
                            onClick={onDarkThemeToggle}
                        >
                            {isDarkTheme ? <WbSunnyIcon /> : <Brightness3Icon />}
                        </IconButton>
                    </Tooltip>
                </div>
            </Toolbar>
        </AppBar>
    );
}
