export default interface INavigationItem {
    label: string;
    route: string;
    matchExactPath?: boolean;
}
