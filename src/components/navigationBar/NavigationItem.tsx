import React from "react";
import { NavLink } from "react-router-dom";
import { makeStyles, createStyles } from "@material-ui/core/styles";

import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";

import INavigationItem from "./models/INavigationItem";

const useStyles = makeStyles((theme) => {
    return createStyles({
        root: {
            width: "unset",
            color: "#d2d2d2",
            "&:hover": {
                color: theme.palette.grey[300],
            },
        },
        activeNavLink: {
            color: theme.palette.common.white,
            "&:hover": {
                color: theme.palette.common.white,
            },
        },
    });
});

interface IProps {
    item: INavigationItem;
}

export default function NavigationItem({ item }: IProps) {
    const classes = useStyles();
    const { label, route, matchExactPath = false } = item;

    return (
        <ListItem
            button
            key={label}
            component={NavLink}
            to={route}
            exact={matchExactPath}
            className={classes.root}
            activeClassName={classes.activeNavLink}
        >
            <ListItemText disableTypography>
                <Typography style={{ fontWeight: "bold" }}>{label}</Typography>
            </ListItemText>
        </ListItem>
    );
}
