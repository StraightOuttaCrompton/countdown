export default class Pool<T> {
    private readonly _initialPool: T[];
    private _currentPool: T[];

    constructor(initialPool: T[]) {
        this._initialPool = initialPool;
        this._currentPool = initialPool;
    }

    public resetPool(): void {
        this._currentPool = this._initialPool;
    }

    public extractItem(): T | undefined {
        if (this._currentPool.length < 1) return undefined;

        const itemIndex = Math.floor(Math.random() * this._currentPool.length);

        const item = this._currentPool.splice(itemIndex, 1)[0];

        return item;
    }
}
