import React from "react";
import ErrorPage from "./ErrorPage";

export default function NotImplementedPage() {
    return <ErrorPage errorCode={501} title="This page has not been implemented" />;
}
