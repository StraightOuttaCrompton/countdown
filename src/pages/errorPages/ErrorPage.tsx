import React from "react";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import useTheme from "@material-ui/core/styles/useTheme";

import PageSection from "components/layout/PageSection";

interface IProps {
    errorCode: number;
    title: string;
    text?: string;
}

export default function ErrorPage({ errorCode, title, text }: IProps) {
    const theme = useTheme();

    return (
        <PageSection>
            <Grid container direction="column" alignItems="center">
                <Grid item>
                    <Typography variant="h1">{errorCode}</Typography>
                </Grid>

                <Grid item>
                    <Typography variant="h5" align="center">
                        {title}
                    </Typography>
                </Grid>

                {typeof text === "undefined" ? null : (
                    <Grid item style={{ marginTop: theme.spacing(1) }}>
                        <Typography variant="body1" align="center">
                            {text}
                        </Typography>
                    </Grid>
                )}
            </Grid>
        </PageSection>
    );
}
