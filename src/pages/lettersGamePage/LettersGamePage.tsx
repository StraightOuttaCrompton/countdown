import React, { useState } from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import clsx from "clsx";

import Pool from "models/Pool";
import getLetterPool from "./getLetterPool";
import consonants from "./letters/consonants";
import vowels from "./letters/vowels";

import Paper from "@material-ui/core/Paper";

import PageSection from "components/layout/PageSection";
import Clock from "components/clock/Clock";
import ItemDisplay from "components/ItemDisplay";
import StyledButton from "components/StyledButton";

const useStyles = makeStyles((theme) => {
    return createStyles({
        letterDisplayContainer: {
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            padding: theme.spacing(1),
            marginBottom: theme.spacing(2),
            "& > *": {
                marginRight: theme.spacing(1),
            },
            "& > *:last-child": {
                marginRight: 0,
            },
            [theme.breakpoints.between(410, 500)]: {
                flexDirection: "column",
                alignItems: "center",
                "& > *": {
                    marginBottom: theme.spacing(1),
                    marginRight: 0,
                },
                "& > *:last-child": {
                    marginBottom: 0,
                },
            },
        },
        topStickyItem: {
            position: "sticky",
            top: +(theme.mixins.toolbar.minHeight ?? 56) + theme.spacing(1),
            zIndex: theme.zIndex.appBar,
        },
        clearButton: {
            [theme.breakpoints.down("xs")]: {
                alignSelf: "flex-end",
            },
        },
        letterButtonContainer: {
            display: "flex",
            justifyContent: "center",
            flexWrap: "wrap",
            marginBottom: theme.spacing(6),
            "& > *": {
                marginRight: theme.spacing(1),
                [theme.breakpoints.down("xs")]: {
                    flex: "1",
                },
            },
            "& > *:last-child": {
                marginRight: 0,
            },
        },
    });
});

export default function LettersGamePage() {
    const classes = useStyles();

    const [vowelPool, setVowelPool] = useState<Pool<string>>(getLetterPool(vowels));
    const [consonantPool, setConsonantPool] = useState<Pool<string>>(getLetterPool(consonants));

    const [selectedLetters, setSelectedLetters] = useState<string[]>([]);
    const [numberOfVowels, setNumberOfVowels] = useState<number>(0);
    const [numberOfConsonants, setNumberOfConsonants] = useState<number>(0);

    // grouping must contain at least three vowels and four consonants
    const maxNumberOfLetters = 9;
    const minNumberOfVowels = 3;
    const minNumberOfConsonants = 4;

    const addVowel = () => {
        let updatedLetters = selectedLetters;

        const extractedItem = vowelPool.extractItem();

        if (typeof extractedItem === "undefined") return;

        updatedLetters.push(extractedItem);

        setSelectedLetters(updatedLetters);
        setNumberOfVowels(numberOfVowels + 1);
    };

    const addConsonant = () => {
        let updatedLetters = selectedLetters;

        const extractedItem = consonantPool.extractItem();

        if (typeof extractedItem === "undefined") return;

        updatedLetters.push(extractedItem);

        setSelectedLetters(updatedLetters);
        setNumberOfConsonants(numberOfConsonants + 1);
    };

    const clearLetters = () => {
        setVowelPool(getLetterPool(vowels));
        setConsonantPool(getLetterPool(consonants));

        setSelectedLetters([]);
        setNumberOfVowels(0);
        setNumberOfConsonants(0);
    };

    return (
        <PageSection>
            <Paper
                className={clsx(classes.letterDisplayContainer, classes.topStickyItem)}
                elevation={2}
            >
                <ItemDisplay items={selectedLetters} maxNumberOfItems={maxNumberOfLetters} />
                <StyledButton
                    onClick={clearLetters}
                    disabled={selectedLetters.length < 1}
                    className={classes.clearButton}
                >
                    Clear
                </StyledButton>
            </Paper>

            <div className={classes.letterButtonContainer}>
                <StyledButton
                    color="primary"
                    onClick={addVowel}
                    disabled={
                        numberOfVowels >= maxNumberOfLetters - minNumberOfConsonants ||
                        selectedLetters.length >= maxNumberOfLetters
                    }
                >
                    Add Vowel
                </StyledButton>
                <StyledButton
                    color="primary"
                    onClick={addConsonant}
                    disabled={
                        numberOfConsonants >= maxNumberOfLetters - minNumberOfVowels ||
                        selectedLetters.length >= maxNumberOfLetters
                    }
                >
                    Add Consonant
                </StyledButton>
            </div>

            <Clock disabled={selectedLetters.length !== maxNumberOfLetters} />
        </PageSection>
    );
}
