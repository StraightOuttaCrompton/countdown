export default interface ILetter {
    frequency: number;
    value: string;
}
