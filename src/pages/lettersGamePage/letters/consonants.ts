import ILetter from "../models/ILetter";

const consonants: ILetter[] = [
    {
        frequency: 2,
        value: "b",
    },
    {
        frequency: 3,
        value: "c",
    },

    {
        frequency: 6,
        value: "d",
    },
    {
        frequency: 2,
        value: "f",
    },
    {
        frequency: 3,
        value: "g",
    },
    {
        frequency: 2,
        value: "h",
    },
    {
        frequency: 1,
        value: "j",
    },
    {
        frequency: 1,
        value: "k",
    },
    {
        frequency: 5,
        value: "l",
    },
    {
        frequency: 4,
        value: "m",
    },
    {
        frequency: 8,
        value: "n",
    },
    {
        frequency: 4,
        value: "p",
    },
    {
        frequency: 1,
        value: "q",
    },
    {
        frequency: 9,
        value: "r",
    },
    {
        frequency: 9,
        value: "s",
    },
    {
        frequency: 9,
        value: "t",
    },
    {
        frequency: 1,
        value: "v",
    },
    {
        frequency: 1,
        value: "x",
    },
    {
        frequency: 1,
        value: "y",
    },
    {
        frequency: 1,
        value: "z",
    },
];

export default consonants;
