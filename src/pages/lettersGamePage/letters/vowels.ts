import ILetter from "../models/ILetter";

const vowels: ILetter[] = [
    {
        frequency: 15,
        value: "a",
    },
    {
        frequency: 21,
        value: "e",
    },
    {
        frequency: 13,
        value: "i",
    },
    {
        frequency: 13,
        value: "o",
    },
    {
        frequency: 5,
        value: "u",
    },
];

export default vowels;
