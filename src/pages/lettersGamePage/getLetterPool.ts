import Pool from "models/Pool";
import ILetter from "./models/ILetter";

export default function getLetterPool(letters: ILetter[]): Pool<string> {
    let _letterPool: string[] = [];

    letters.forEach((item) => {
        const { value, frequency } = item;

        if (value.length !== 1) return;

        const upperCaseValue = value.toUpperCase();

        for (let i = 0; i < frequency; i++) {
            _letterPool.push(upperCaseValue);
        }
    });

    return new Pool(_letterPool);
}
