import React from "react";
import ITargetNumber from "./ITargetNumber";
import StyledButton from "components/StyledButton";

interface IProps {
    disabled?: boolean;
    onTargetNumberGenerated?: (targetNumber: ITargetNumber) => void;
}

export default function TargetNumberGeneratorButton(props: IProps) {
    const { disabled = false, onTargetNumberGenerated = () => {} } = props;

    const generateNumber = () => {
        const digitOne = Math.floor(Math.random() * 9 + 1); // between 1 and 9
        const digitTwo = Math.floor(Math.random() * 10); // between 0 and 9
        const digitThree = Math.floor(Math.random() * 10); // between 0 and 9

        onTargetNumberGenerated({
            digitOne: digitOne,
            digitTwo: digitTwo,
            digitThree: digitThree,
        });
    };

    return (
        <StyledButton color="primary" onClick={generateNumber} disabled={disabled}>
            Generate Target Number
        </StyledButton>
    );
}
