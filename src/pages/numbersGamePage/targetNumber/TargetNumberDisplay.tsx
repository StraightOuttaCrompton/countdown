import React, { useState, useEffect, HTMLAttributes } from "react";
import sleep from "utils/sleep";

import ITargetNumber from "./ITargetNumber";
import ItemDisplay from "components/ItemDisplay";

interface IProps extends HTMLAttributes<HTMLDivElement> {
    targetNumber?: ITargetNumber;
}

export default function TargetNumberDisplay(props: IProps) {
    const { targetNumber, ...rest } = props;

    const [digitOne, setDigitOne] = useState<number>();
    const [digitTwo, setDigitTwo] = useState<number>();
    const [digitThree, setDigitThree] = useState<number>();

    useEffect(() => {
        const displayTargetNumber = async () => {
            const numberOfIntermediateNumbers = 8;
            const sleepTime = 30;

            for (let i = 0; i < numberOfIntermediateNumbers; i++) {
                const digitOne = Math.floor(Math.random() * 9 + 1); // between 1 and 9

                setDigitOne(digitOne);

                await sleep(sleepTime);
            }

            for (let i = 0; i < numberOfIntermediateNumbers; i++) {
                const digitTwo = Math.floor(Math.random() * 10); // between 0 and 9

                setDigitTwo(digitTwo);

                await sleep(sleepTime);
            }

            for (let i = 0; i < numberOfIntermediateNumbers; i++) {
                const digitThree = Math.floor(Math.random() * 10); // between 0 and 9

                setDigitThree(digitThree);

                await sleep(sleepTime);
            }
        };

        if (typeof targetNumber === "undefined") {
            setDigitOne(undefined);
            setDigitTwo(undefined);
            setDigitThree(undefined);
        } else {
            displayTargetNumber();
        }
    }, [targetNumber]);

    return (
        <ItemDisplay
            items={[digitOne?.toString(), digitTwo?.toString(), digitThree?.toString()]}
            maxNumberOfItems={3}
            {...rest}
        />
    );
}
