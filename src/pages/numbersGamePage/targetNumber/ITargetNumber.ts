export default interface ITargetNumber {
    digitOne: number;
    digitTwo: number;
    digitThree: number;
}
