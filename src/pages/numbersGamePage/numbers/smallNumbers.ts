import INumber from "../models/INumber";

const smallNumbers: INumber[] = [
    {
        value: 1,
        frequency: 2,
    },
    {
        value: 2,
        frequency: 2,
    },
    {
        value: 3,
        frequency: 2,
    },
    {
        value: 4,
        frequency: 2,
    },
    {
        value: 5,
        frequency: 2,
    },
    {
        value: 6,
        frequency: 2,
    },
    {
        value: 7,
        frequency: 2,
    },
    {
        value: 8,
        frequency: 2,
    },
    {
        value: 9,
        frequency: 2,
    },
];

export default smallNumbers;
