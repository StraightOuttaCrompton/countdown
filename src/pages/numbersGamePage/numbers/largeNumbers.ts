import INumber from "../models/INumber";

const largeNumbers: INumber[] = [
    {
        value: 25,
        frequency: 1,
    },
    {
        value: 50,
        frequency: 1,
    },
    {
        value: 75,
        frequency: 1,
    },
    {
        value: 100,
        frequency: 1,
    },
];

export default largeNumbers;
