import Pool from "models/Pool";
import INumber from "./models/INumber";

export default function getNumberPool(numbers: INumber[]): Pool<number> {
    let _numberPool: number[] = [];

    numbers.forEach((item) => {
        const { value, frequency } = item;

        for (let i = 0; i < frequency; i++) {
            _numberPool.push(value);
        }
    });

    return new Pool(_numberPool);
}
