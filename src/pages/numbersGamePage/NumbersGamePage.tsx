import React, { useState } from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import clsx from "clsx";

import Pool from "models/Pool";
import getNumberPool from "./getNumberPool";
import smallNumbers from "./numbers/smallNumbers";
import largeNumbers from "./numbers/largeNumbers";

import Paper from "@material-ui/core/Paper";

import PageSection from "components/layout/PageSection";
import ItemDisplay from "components/ItemDisplay";
import Clock from "components/clock/Clock";
import StyledButton from "components/StyledButton";

import TargetNumberGeneratorButton from "./targetNumber/TargetNumberGeneratorButton";
import ITargetNumber from "./targetNumber/ITargetNumber";
import TargetNumberDisplay from "./targetNumber/TargetNumberDisplay";

const useStyles = makeStyles((theme) => {
    return createStyles({
        numberDisplayContainer: {
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            padding: theme.spacing(1),
            marginBottom: theme.spacing(2),
            "& > *": {
                marginRight: theme.spacing(1),
            },
            "& > *:last-child": {
                marginRight: 0,
            },
            [theme.breakpoints.down(400)]: {
                flexDirection: "column",
                alignItems: "center",
                "& > *": {
                    marginBottom: theme.spacing(1),
                    marginRight: 0,
                },
                "& > *:last-child": {
                    marginBottom: 0,
                },
            },
        },
        topStickyItem: {
            position: "sticky",
            top: +(theme.mixins.toolbar.minHeight ?? 56) + theme.spacing(1),
            zIndex: theme.zIndex.appBar,
        },
        clearButton: {
            [theme.breakpoints.down("xs")]: {
                alignSelf: "flex-end",
            },
        },
        numberButtonContainer: {
            display: "flex",
            justifyContent: "center",
            flexWrap: "wrap",
            marginBottom: theme.spacing(2),
            "& > *": {
                marginRight: theme.spacing(1),
                [theme.breakpoints.down("xs")]: {
                    flex: "1",
                },
            },
            "& > *:last-child": {
                marginRight: 0,
            },
        },
        quickNumberButtonContainer: {
            display: "grid",
            gridGap: theme.spacing(1),
            gridTemplateColumns: "repeat(4, 1fr)",
            marginBottom: theme.spacing(6),
            [theme.breakpoints.down("xs")]: {
                gridTemplateColumns: "repeat(2, 1fr)",
            },
        },
        targetNumberContainer: {
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            marginBottom: theme.spacing(2),
            padding: theme.spacing(1),
            "& > *": {
                marginRight: theme.spacing(1),
            },
            "& > *:last-child": {
                marginRight: 0,
            },
            [theme.breakpoints.down("xs")]: {
                flexDirection: "column",
                "& > *": {
                    marginBottom: theme.spacing(1),
                    marginRight: 0,
                },
                "& > *:last-child": {
                    marginBottom: 0,
                },
            },
        },
    });
});

export default function NumbersGamePage() {
    const classes = useStyles();

    const [smallNumberPool, setSmallNumberPool] = useState<Pool<number>>(
        getNumberPool(smallNumbers)
    );
    const [largeNumberPool, setLargeNumberPool] = useState<Pool<number>>(
        getNumberPool(largeNumbers)
    );

    const [selectedNumbers, setSelectedNumbers] = useState<number[]>([]);
    const [numberOfSmallNumbers, setNumberOfSmallNumbers] = useState<number>(0);
    const [numberOfLargeNumbers, setNumberOfLargeNumbers] = useState<number>(0);

    const [targetNumber, setTargetNumber] = useState<ITargetNumber>();

    const maxNumberOfNumbers = 6;

    const addSmallNumber = () => {
        let updatedNumbers = selectedNumbers;

        const extractedItem = smallNumberPool.extractItem();

        if (typeof extractedItem === "undefined") return;

        updatedNumbers.push(extractedItem);

        setSelectedNumbers(updatedNumbers);
        setNumberOfSmallNumbers(numberOfSmallNumbers + 1);
    };

    const addLargeNumber = () => {
        let updatedNumbers = selectedNumbers;

        const extractedItem = largeNumberPool.extractItem();

        if (typeof extractedItem === "undefined") return;

        updatedNumbers.push(extractedItem);

        setSelectedNumbers(updatedNumbers);
        setNumberOfLargeNumbers(numberOfLargeNumbers + 1);
    };

    const onClearClick = () => {
        setSmallNumberPool(getNumberPool(smallNumbers));
        setLargeNumberPool(getNumberPool(largeNumbers));

        setSelectedNumbers([]);
        setNumberOfSmallNumbers(0);
        setNumberOfLargeNumbers(0);
        setTargetNumber(undefined);
    };

    const addNLargeNumbers = (numberOfLargeNumbers: number) => {
        for (let i = 0; i < numberOfLargeNumbers; i++) {
            addLargeNumber();
        }

        while (selectedNumbers.length < maxNumberOfNumbers) {
            addSmallNumber();
        }
    };

    return (
        <PageSection>
            <Paper
                className={clsx(classes.numberDisplayContainer, classes.topStickyItem)}
                elevation={2}
            >
                <ItemDisplay
                    items={selectedNumbers.map((item) => item.toString())}
                    maxNumberOfItems={maxNumberOfNumbers}
                />
                <StyledButton
                    onClick={onClearClick}
                    disabled={selectedNumbers.length < 1}
                    className={classes.clearButton}
                >
                    Clear
                </StyledButton>
            </Paper>

            <div className={classes.numberButtonContainer}>
                <StyledButton
                    color="primary"
                    onClick={addSmallNumber}
                    disabled={
                        selectedNumbers.length >= maxNumberOfNumbers ||
                        numberOfSmallNumbers === smallNumbers.length
                    }
                >
                    Add Small Number
                </StyledButton>
                <StyledButton
                    color="primary"
                    onClick={addLargeNumber}
                    disabled={
                        selectedNumbers.length >= maxNumberOfNumbers ||
                        numberOfLargeNumbers === largeNumbers.length
                    }
                >
                    Add Large Number
                </StyledButton>
            </div>

            <div className={classes.quickNumberButtonContainer}>
                <StyledButton
                    onClick={() => addNLargeNumbers(1)}
                    disabled={selectedNumbers.length > 0}
                >
                    1 Large
                </StyledButton>

                <StyledButton
                    onClick={() => addNLargeNumbers(2)}
                    disabled={selectedNumbers.length > 0}
                >
                    2 Large
                </StyledButton>

                <StyledButton
                    onClick={() => addNLargeNumbers(3)}
                    disabled={selectedNumbers.length > 0}
                >
                    3 Large
                </StyledButton>

                <StyledButton
                    onClick={() => addNLargeNumbers(4)}
                    disabled={selectedNumbers.length > 0}
                >
                    4 Large
                </StyledButton>
            </div>

            <Paper className={classes.targetNumberContainer}>
                <TargetNumberGeneratorButton
                    onTargetNumberGenerated={(_targetNumber: ITargetNumber) =>
                        setTargetNumber(_targetNumber)
                    }
                    disabled={
                        selectedNumbers.length !== maxNumberOfNumbers ||
                        typeof targetNumber !== "undefined"
                    }
                />

                <TargetNumberDisplay targetNumber={targetNumber} />
            </Paper>

            <Clock
                disabled={
                    selectedNumbers.length !== maxNumberOfNumbers ||
                    typeof targetNumber === "undefined"
                }
            />
        </PageSection>
    );
}
