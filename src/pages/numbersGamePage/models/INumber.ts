export default interface INumber {
    frequency: number;
    value: number;
}
