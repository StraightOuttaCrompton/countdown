import React from "react";

import Typography from "@material-ui/core/Typography";

import PageSection from "components/layout/PageSection";
import NewTabLink from "components/links/NewTabLink";

export default function CreditsPage() {
    const links = {
        countdownPractice: "https://incoherency.co.uk/countdown/practice/",
        letterFrequencies: "http://www.thecountdownpage.com/letters.htm",
        wiki: {
            lettersRound: "https://en.wikipedia.org/wiki/Countdown_(game_show)#Letters_round",
            numbersRound: "https://en.wikipedia.org/wiki/Countdown_(game_show)#Numbers_round",
        },
    };

    return (
        <PageSection>
            <Typography variant="subtitle1">Resources</Typography>
            <Typography variant="subtitle2">
                Music and clock from{" "}
                <NewTabLink href={links.countdownPractice}>
                    James Stanley's countdown page
                </NewTabLink>
                .
            </Typography>
            <Typography variant="subtitle2">
                Letters frequensies obtained from{" "}
                <NewTabLink href={links.letterFrequencies}>The Countdown Page</NewTabLink>.
            </Typography>
            <Typography variant="subtitle2">
                Rules for the <NewTabLink href={links.wiki.lettersRound}>letters page</NewTabLink>{" "}
                and <NewTabLink href={links.wiki.numbersRound}>numbers page</NewTabLink> comply with
                wikipedia.
            </Typography>
        </PageSection>
    );
}
