import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import routes from "routes";

import PageLayout from "components/layout/PageLayout";

import NotFoundPage from "pages/errorPages/NotFoundPage";
import LettersGamePage from "pages/lettersGamePage/LettersGamePage";
import NumbersGamePage from "pages/numbersGamePage/NumbersGamePage";
import CreditsPage from "pages/creditsPage/CreditsPage";

function App() {
    return (
        <PageLayout>
            <Switch>
                {routes.homePage !== "/" ? (
                    <Route exact path="/" render={() => <Redirect to={routes.homePage} />} />
                ) : null}
                <Route exact path={routes.lettersGame} render={() => <LettersGamePage />} />
                <Route exact path={routes.numbersGame} render={() => <NumbersGamePage />} />
                <Route exact path={routes.creditsPage} render={() => <CreditsPage />} />
                <Route render={() => <NotFoundPage />} />
            </Switch>
        </PageLayout>
    );
}

export default App;
