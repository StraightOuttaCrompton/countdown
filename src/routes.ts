const lettersGameRoute = "/letters";

const routes = {
    homePage: lettersGameRoute,
    lettersGame: lettersGameRoute,
    numbersGame: "/numbers",
    creditsPage: "/credits",
};

export default routes;
